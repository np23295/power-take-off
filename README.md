# PowerTakeOffApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.12.

## Setup Project: 

1. Execute `npm install` to install all dependencies.
2. `ng serve` to run the project

## Task 

1. Create angular 9 app - DONE
2. Add bootstrap or Material library - DONE
3. Add routing (ex. Add few pages Dashboard, setting, about us pages), NOTE: Angular app should have more than 1 module - DONE
4. Call any backend open API (REST post-call) by a button click and show the result on the dashboard(use ngFor directive if you get a response in Array or list), NOTE: use Rxjs as much complex way as you can (you can add it on the same page or on the other page of the application) - DONE
5. background of dashboard page should change every 5 seconds - DONE
6. write a unit test for any component (optional) - Unable to accomplish due to time constraint 
7. We need one input box control on the dashboard - DONE - manual input is currently disabled, but arrows, keyboard and scroll works


## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
