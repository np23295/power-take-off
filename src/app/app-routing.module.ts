import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AboutUsComponent } from './navigation/about-us/about-us.component';
import { DashboardComponent } from './navigation/dashboard/dashboard.component';
import { SettingsComponent } from './navigation/settings/settings.component';
import { UsersComponent } from './navigation/users/users.component';


const routes: Routes = [
  { path: '', component: DashboardComponent },
  { path: 'settings', component: SettingsComponent },
  { path: 'about-us', component: AboutUsComponent },
  { path: 'users', component: UsersComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
