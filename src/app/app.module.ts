import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatMenuModule } from '@angular/material/menu';
import { MatButtonModule } from '@angular/material/button';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { UsersComponent } from './navigation/users/users.component';
import { MatListModule } from '@angular/material/list';
import { MatIconModule } from '@angular/material/icon';
import { AddUserFormComponent } from './navigation/add-user-form/add-user-form.component';
import { FormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { HeaderComponent } from './header/header.component';
import { DashboardComponent } from './navigation/dashboard/dashboard.component';
import { SettingsComponent } from './navigation/settings/settings.component';
import { AboutUsComponent } from './navigation/about-us/about-us.component';
import { TimeInputComponent } from './widgets/time-input/time-input.component';
import { ManageTimeDirective } from './directives/manage-time.directive';

@NgModule({
  declarations: [
    AppComponent, UsersComponent, AddUserFormComponent, HeaderComponent, DashboardComponent, SettingsComponent, AboutUsComponent, TimeInputComponent, ManageTimeDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatMenuModule,
    MatButtonModule,
    HttpClientModule,
    CommonModule,
    MatListModule,
    MatIconModule,
    FormsModule,
    MatInputModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
