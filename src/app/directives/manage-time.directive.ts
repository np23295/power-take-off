import { Directive, Input, HostListener } from "@angular/core";

export enum KEY_CODE {
    UP_ARROW = 38,
    DOWN_ARROW = 40
}

const MAX_HOURS_IN_A_DAY = 24;
const MAX_MINUTES_IN_AN_HOUR = 60;

@Directive({
    selector: "[inputTimeFilter]"
})
export class ManageTimeDirective {
    @Input('inputTimeFilter') numberOfMinutes: number;

    @HostListener("mousewheel", ["$event"]) onMousewheel($event) {
        if ($event.wheelDelta > 0) this.setIncreasedTime($event);
        else this.setDecreasedTime($event);
    }

    @HostListener("window:keyup", ["$event"])
    onKeyUp($event: KeyboardEvent) {
        if ($event.keyCode === KEY_CODE.UP_ARROW) {
            this.setIncreasedTime($event);
        } else if ($event.keyCode === KEY_CODE.DOWN_ARROW) {
            this.setDecreasedTime($event);
        }
    }

    getMinutes($event) {
        const hours = parseInt($event.target.value.split(":")[0]);
        const totalMinutes = hours * MAX_MINUTES_IN_AN_HOUR;

        const remainingMinutes = $event.target.value.split(":")[1];

        return totalMinutes + parseInt(remainingMinutes);
    }

    setDecreasedTime($event) {
        const totalMinutes = this.getMinutes($event) - this.numberOfMinutes;
        this.setTime(totalMinutes, $event);
    }

    setIncreasedTime($event) {
        const totalMinutes = this.getMinutes($event) + this.numberOfMinutes;
        this.setTime(totalMinutes, $event);
    }

    setTime(totalMinutes: number, $event) {
        let totalHours = Math.trunc(totalMinutes / MAX_MINUTES_IN_AN_HOUR);
        let remainingMinutes = totalMinutes % MAX_MINUTES_IN_AN_HOUR;

        if (totalHours >= MAX_HOURS_IN_A_DAY) {
            totalHours = 0;
        }
        if (totalHours < 0) {
            totalHours = 0;
        }
        if (remainingMinutes < 0) {
            remainingMinutes = 0;
        }
        $event.target.value = `${totalHours.toString().length > 1 ? totalHours : `0${totalHours}`
            }:${remainingMinutes.toString().length > 1
                ? remainingMinutes
                : `0${remainingMinutes}`
            }`;
    }
}
