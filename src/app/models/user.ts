import { Address } from './address';
import { Company } from './company';

export class User {

  constructor(name, email) {
    this.name = name;
    this.email = email;
  }

  public id: number;
  public name: string;
  public username: string;
  public email: string;
  public address: Address;
  public phone: number;
  public website: string;
  public company: Company;
}



