import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/user';
import { UsersService } from '../users/users.service';

@Component({
  selector: 'app-add-user-form',
  templateUrl: './add-user-form.component.html',
  styleUrls: ['./add-user-form.component.css']
})
export class AddUserFormComponent implements OnInit {

  model = new User('', '');

  constructor(private userService: UsersService) { }

  ngOnInit(): void {
  }

  // Add a new user to service
  addUser(user: User) {
    this.userService.addUser(user);
  }

  // Called when form is submitted
  onSubmit() {
    this.addUser(this.model);
    this.newUser();
  }

  newUser() {
    this.model = new User('', '');
  }
}
