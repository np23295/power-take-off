import { Component, ElementRef, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  background = ['white', 'skyblue'];
  onDestroyFlag = false;
  currentTime: string;

  constructor(private el: ElementRef) { }

  ngOnInit() {
    const date = new Date();
    const currentMinutes = date.getMinutes();
    const minutesToBeDisplayed =
      currentMinutes.toString().length > 1
        ? currentMinutes
        : `0${currentMinutes}`;

    this.currentTime = date.getHours() + ":" + minutesToBeDisplayed;
  }

  // After View init
  ngAfterViewInit() {
    this.onDestroyFlag = false;
    this.timeout();
  }

  timeout() {
    var that = this;
    setTimeout(function () {
      if (!that.onDestroyFlag) {
        that.changeColor();
        that.timeout();
      }
    }, 5000);
  }

  changeColor() {
    const random = Math.floor(Math.random() * this.background.length);
    this.el.nativeElement.ownerDocument.body.style.backgroundColor = this.background[random];
  }

  ngOnDestroy() {
    this.onDestroyFlag = true;
    this.el.nativeElement.ownerDocument.body.style.backgroundColor = 'white';
  }
}
