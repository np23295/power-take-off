import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { NavigationRoutingModule } from './navigation-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { MatMenuModule } from '@angular/material/menu';
import { MatButtonModule } from '@angular/material/button';
import { SettingsComponent } from './settings/settings.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { UsersComponent } from './users/users.component';
import { HttpClientModule } from '@angular/common/http';
import { MatListModule } from '@angular/material/list';
import { MatIconModule } from '@angular/material/icon';
import { AddUserFormComponent } from './add-user-form/add-user-form.component';
import { FormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { TimeInputComponent, MyDirective } from '../widgets/time-input/time-input.component';
import { ManageTimeDirective } from '../directives/manage-time.directive';

@NgModule({
  declarations: [DashboardComponent, SettingsComponent, AboutUsComponent, UsersComponent, AddUserFormComponent, TimeInputComponent, ManageTimeDirective],
  imports: [
    BrowserModule,
    CommonModule,
    NavigationRoutingModule,
    MatButtonModule,
    MatMenuModule,
    HttpClientModule,
    MatListModule,
    MatIconModule,
    FormsModule,
    MatInputModule,
  ]
})
export class NavigationModule { }
