import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { User } from 'src/app/models/user';
import { UsersService } from './users.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})

export class UsersComponent implements OnDestroy {
  users: User[];
  subscription: Subscription;

  constructor(userService: UsersService) {
    this.subscription = userService.retrieveUsers().subscribe(users => {
      if (users) {
        this.users = users;
      } else {
        this.users = [];
      }
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
