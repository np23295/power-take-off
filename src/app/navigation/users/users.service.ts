import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of, Subject, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { User } from 'src/app/models/user';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  baseUsersUrl = 'https://jsonplaceholder.typicode.com/users';

  private subject = new BehaviorSubject<User[]>([]);

  constructor(private http: HttpClient) {
    this.getUsersFromServer();
  }

  getUsersFromServer() {
    this.http.get<User[]>(this.baseUsersUrl)
      .pipe(
        catchError(err => of([]))
      ).subscribe(users => { this.subject.next(users); });
  }

  addUser(user: User) {
    this.subject.getValue().push(user);
  }

  retrieveUsers(): Observable<User[]> {
    return this.subject.asObservable();
  }
}
