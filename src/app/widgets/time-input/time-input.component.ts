import { Component, Input, Output } from '@angular/core';
import { EventEmitter } from '@angular/core';

const MAX_HOURS_IN_A_DAY = 24;
const MAX_MINUTES_IN_AN_HOUR = 60;
const numberOfMinutes = 45;

@Component({
  selector: 'app-time-input',
  templateUrl: './time-input.component.html',
  styleUrls: ['./time-input.component.css']
})

export class TimeInputComponent {
  @Input() time: string;

  getMinutes() {
    const hours = parseInt(this.time.split(":")[0]);
    const totalMinutes = hours * MAX_MINUTES_IN_AN_HOUR;

    const remainingMinutes = this.time.split(":")[1];

    return totalMinutes + parseInt(remainingMinutes);
}

setDecreasedTime(time) {
  console.log(time, this.time)
    const totalMinutes = this.getMinutes() - numberOfMinutes;
    this.setTime(totalMinutes);
}

setIncreasedTime(time) {
  console.log(time, this.time)
    const totalMinutes = this.getMinutes() + numberOfMinutes;
    this.setTime(totalMinutes);
}

setTime(totalMinutes: number) {
    let totalHours = Math.trunc(totalMinutes / MAX_MINUTES_IN_AN_HOUR);
    let remainingMinutes = totalMinutes % MAX_MINUTES_IN_AN_HOUR;

    if (totalHours >= MAX_HOURS_IN_A_DAY) {
        totalHours = 0;
    }
    if (totalHours < 0) {
        totalHours = 0;
    }
    if (remainingMinutes < 0) {
        remainingMinutes = 0;
    }
    this.time = `${totalHours.toString().length > 1 ? totalHours : `0${totalHours}`
        }:${remainingMinutes.toString().length > 1
            ? remainingMinutes
            : `0${remainingMinutes}`
        }`;
}
}
